package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();
   private Greeter g2 = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }


   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("Test for Name='<your name>'")
   public void nameGreeter(){
       g.setName("<your name>");
       assertEquals(g.getName(), "<your name>");
       assertEquals(g.sayHello(), "Hello <your name>!");
   }

   @Test
   @DisplayName("Test for Andrew")
   public void testGreeterAndrew(){
       g.setName("Andrew");
       assertEquals(g.getName(), "Andrew");
       assertEquals(g.sayHello(), "Hello Andrew!");
       assertFalse(g.getName().equals("Jimmy"), "Asserted that Andrew does not equal Jimmy");
   }

   @Test
   @DisplayName("Test for Name='Lee'")
   public void testGreeterLee() 
   {

      g.setName("Lee");
      assertEquals(g.getName(),"Lee");
      assertEquals(g.sayHello(),"Hello Lee!");
   }
   
   @Test
   @DisplayName("Testing multiple greeters")
   public void testGreeterMultiple() 
   {

      g.setName("Test");
      g2.setName("Test");
      assertFalse(g == g2);
   }

   @Test
   @DisplayName("Test for Name= Roldan")
   public void testGreeterRoldan(){
      g.setName("Roldan");
      assertEquals(g.getName(), "Roldan");
      assertEquals(g.sayHello(), "Hello Roldan!");


    }
    @Test
    @DisplayName("Test for assertsFalse - Roldan")
    public void testGreeterAF_Roldan() {
       assertFalse(1 == 5);
       assertFalse(2 == 20);
    }


    @Test
    @DisplayName("Lee's test for Assignment 14")
    public void a14test() {
       assertSame("thing", "thing");
    }

    @Test
    @DisplayName("Roldan's Test for A14")
    public void a14_Test(){
       assertNotSame("Same", "NotSame");
    }

    
}
